/* 
 * COPYRIGHT (C) 2018 VANGIEX
 *
 * THIS PROGRAM IS FREE SOFTWARE: YOU CAN REDISTRIBUTE IT AND/OR MODIFY
 * IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
 * THE FREE SOFTWARE FOUNDATION, EITHER VERSION 3 OF THE LICENSE, OR
 * (AT YOUR OPTION) ANY LATER VERSION.
 *
 * THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL,
 * BUT WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
 * MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  SEE THE
 * GNU GENERAL PUBLIC LICENSE FOR MORE DETAILS.
 *
 * YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
 * ALONG WITH THIS PROGRAM.  IF NOT, SEE <HTTP://WWW.GNU.ORG/LICENSES/>.
 */
/**
 * AUTHOR:  VANGIEX
 * CREATED: 7 MAR, 2018
 */

CREATE TABLE IF NOT EXISTS  USERS(
                            UID INT NOT NULL AUTO_INCREMENT,
                            FNAME VARCHAR(100), 
                            LNAME VARCHAR(100), 
                            EMAIL VARCHAR(100), 
                            PHONE VARCHAR(10), 
                            BIO VARCHAR(100),
                            FACEBOOK VARCHAR(255), 
                            TWITER VARCHAR(255),
                            primary key (UID),
                            UNIQUE KEY(TWITER,FACEBOOK,EMAIL,PHONE)
);

CREATE TABLE IF NOT EXISTS  LOGIN(
                            UID INT NOT NULL , 
                            USERNAME VARCHAR(100), 
                            PASSWORD VARCHAR(100), 
                            primary key (UID,USERNAME)
);


CREATE TABLE IF NOT EXISTS  VMCONFIGPOOL(
                            VID INT NOT NULL AUTO_INCREMENT, 
                            UID INT NOT NULL, 
                            VM_NAME CHAR(50),
                            IP INT(100),
                            PORT INT(100), 
                            PROTOCAL VARCHAR(30),
                            primary key (VID),
                            UNIQUE KEY (UID,VM_NAME,PORT)
); 

CREATE TABLE IF NOT EXISTS  VMDETAILSPOOL(
                            VID INT NOT NULL, 
                            OS VARCHAR(100), 
                            NETWORK_SUPPORT VARCHAR(100), 
                            USERNAME VARCHAR(100), 
                            PASSWORD VARCHAR(100),
                            primary key (VID),
                            UNIQUE KEY(USERNAME,PASSWORD)
); 

CREATE TABLE IF NOT EXISTS  TODOLIST(
                            TID INT NOT NULL AUTO_INCREMENT, 
                            UID INT NOT NULL, 
                            NOTE VARCHAR(1000), 
                            STATUS VARCHAR(100),
                            TIMESTMP TIME,
                            primary key (TID)
); 

CREATE TABLE IF NOT EXISTS  HISTORY(
                            HID INT NOT NULL AUTO_INCREMENT,
                            UID INT NOT NULL,
                            VID INT,
                            ACTIVITY VARCHAR(1000),
                            ATIME TIME, 
                            primary key (HID)
); 

CREATE TABLE IF NOT EXISTS  FRIENDLIST(
                            FID INT NOT NULL AUTO_INCREMENT, 
                            UID INT NOT NULL, 
                            FWID INT NOT NULL, 
                            TIMESTMP TIME,
                            primary key (FID),
                            UNIQUE KEY (UID,FWID)
); 


CREATE TABLE IF NOT EXISTS  COMMUNITY(
                            CID INT NOT NULL AUTO_INCREMENT, 
                            ASKED_UID INT NOT NULL, 
                            REPLIED_UID INT NOT NULL, 
                            TIMESTMP TIME, 
                            STATUS VARCHAR(100),
                            primary key (CID)
);

CREATE TABLE IF NOT EXISTS  ASKED(
                            ASKED_UID INT NOT NULL,
                            UID INT NOT NULL,
                            QOESTION VARCHAR(1000), 
                            TIMESTMP TIME,
                            primary key (ASKED_UID)
); 

CREATE TABLE IF NOT EXISTS  REPLIED( 
                            REPLIED_UID  INT NOT NULL,
                            UID INT NOT NULL,
                            ANSWER VARCHAR(1000), 
                            TIMESTMP TIME,
                            primary key (REPLIED_UID)
);